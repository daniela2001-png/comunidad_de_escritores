from django.db import models
from .fic import Fic


class Comentario(models.Model):
    id = models.BigAutoField(primary_key=True)
    fk_id_fic = models.ForeignKey(
        Fic, related_name='_idFic', on_delete=models.CASCADE)
    reaccion = models.CharField('_Reaccion', max_length=45)
    calificacion = models.CharField('_Calificación', max_length=45)
