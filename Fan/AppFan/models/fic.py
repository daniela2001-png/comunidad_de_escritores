from django.db import models
from .usuario import User


class Fic(models.Model):
    idFic = models.BigAutoField(primary_key=True)
    fk_id_autor = models.ForeignKey(
        User, related_name='_fk_id_autor', on_delete=models.CASCADE)
    titulo = models.CharField('_titulo', max_length=150)
    fecha_publicacion = models.DateTimeField()
    genero_literario = models.CharField('_genero_literario', max_length=45)
    idioma = models.CharField('_idioma', max_length=10)
    epilogo = models.CharField('_epilogo', max_length=255)
