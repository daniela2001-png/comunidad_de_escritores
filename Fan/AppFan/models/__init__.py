from django.contrib import admin

from .comentario import Comentario
from .usuario import User
from .fic import Fic

admin.site.register(Comentario)
admin.site.register(User)
admin.site.register(Fic)