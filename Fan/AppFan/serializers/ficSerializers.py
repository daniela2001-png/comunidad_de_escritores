from AppFan.models.fic import Fic
from rest_framework import serializers
from AppFan.models.usuario import User
from AppFan.serializers.usuarioSerializers import usuarioSerializers


class FicSerializer(serializers.ModelSerializer):
    usuario = usuarioSerializers

    class Meta:
        model = Fic
        fields = '__all__'

    # def to_representation(self, obj):
    #     user = User.objects.get(id=obj.id)
    #     fic = Fic.objects.get(user=obj.id)
    #     return {
    #         "username": user.username,
    #         "email": user.email,
    #         "id_fic": fic.idFic
    #     }
