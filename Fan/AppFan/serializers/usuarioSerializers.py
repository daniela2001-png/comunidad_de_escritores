from django.db import models
from django.db.models import fields
from rest_framework import serializers
from AppFan.models.usuario import User


class usuarioSerializers(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'
