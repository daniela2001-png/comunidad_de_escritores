from django.http import HttpResponse
from rest_framework.renderers import JSONRenderer
from AppFan.models import Fic, User
from AppFan.serializers import FicSerializer, usuarioSerializers
import json


def show_all_fics(request):
    content = dict()
    queryset = Fic.objects.all()
    lista = []
    for item in queryset:
        fic_item = FicSerializer(item)
        lista.append(fic_item.data)
    content["data"] = lista
    json_data = JSONRenderer().render(lista)
    return HttpResponse(json_data)


def filter_fics(request):
    queryset = Fic.objects.all()
    lista = []
    for item in queryset:
        fic_item = FicSerializer(item)
        lista.append(fic_item.data)
    authors = User.objects.all()
    list_authors = []
    for author in authors:
        object_author = usuarioSerializers(author)
        list_authors.append(object_author.data)
    if request.GET.get("autor") is not None and request.GET.get("titulo") is not None:
        list_names_authors = []
        list_ids_authros = []
        dict_fic_autor = {}
        for author_name in list_authors:
            if author_name.get("username") is not None and author_name.get("id") is not None:
                dict_fic_autor[author_name.get(
                    "username")] = author_name.get("id")
                list_names_authors.append(author_name.get("username"))
                list_ids_authros.append(author_name.get("id"))
        list_fics_by_title = []
        if request.GET.get("autor") in list_names_authors:
            if request.GET.get("autor") in dict_fic_autor.keys():
                for fic in lista:
                    if fic.get("fk_id_autor") == dict_fic_autor[request.GET.get("autor")] and fic.get("titulo") == request.GET.get("titulo"):
                        list_fics_by_title.append(fic)
                return HttpResponse(json.dumps(list_fics_by_title))
        else:
            return HttpResponse(json.dumps({"error": "el autor dado no existe", "status_code": "404"}))
    elif request.GET.get("autor") is not None:
        dict_fic_autor = {}
        for author_name in list_authors:
            if author_name.get("username") is not None and author_name.get("id") is not None:
                dict_fic_autor[author_name.get(
                    "username")] = author_name.get("id")
        list_fics_by_author = []
        for fic in lista:
            if fic.get("fk_id_autor") == dict_fic_autor[request.GET.get("autor")]:
                list_fics_by_author.append(fic)
        return HttpResponse(json.dumps(list_fics_by_author))
    else:
        json_data = show_all_fics(request)
        return json_data
