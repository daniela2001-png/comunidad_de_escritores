from rest_framework import status, views
from AppFan.serializers import ComentarioSerializer
from rest_framework.response import Response


class ComentarioVista(views.APIView):
    def post(self, request, *args, **kwargs):
        var = request.data
        var["fk_id_fic"] = kwargs["id_fic"]
        serializer = ComentarioSerializer(data=var)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response("El comentario se ha creado", status=status.HTTP_201_CREATED)
