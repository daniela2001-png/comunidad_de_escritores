from django.http import HttpResponse
from rest_framework.renderers import JSONRenderer
from AppFan.models import Fic
from AppFan.serializers import FicSerializer
import json


def show_info_fic(request, pk):
    try:
        queryset = Fic.objects.get(idFic=pk)
        fic_object = FicSerializer(queryset)
        json_data = JSONRenderer().render(fic_object.data)
    except Exception as e:
        return HttpResponse(json.dumps({"error": "el fic no existe", "status_code": "404"}))
    return HttpResponse(json_data)
