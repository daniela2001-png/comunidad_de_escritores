from .userCreateView import UserCreateView
from .userDetailView import UserDetailView
from .ficCreateView import ficCreateView
from .ficDetailView import ficDetailView
from .FicInfo import show_info_fic
from .FilterFics import filter_fics
