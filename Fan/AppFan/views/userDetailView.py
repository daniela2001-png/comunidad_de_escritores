from django.conf import settings
from django.http.response import HttpResponse
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated
from AppFan.models.usuario import User
from AppFan.serializers.usuarioSerializers import usuarioSerializers
from rest_framework_simplejwt.tokens import AccessToken
from rest_framework.renderers import JSONRenderer
import json

class UserDetailView(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = usuarioSerializers
    # permission_classes = (IsAuthenticated,)
    def get(self, request, *args, **kwargs):
        try:
            token_str = kwargs["token"]
            tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
            valid_data = tokenBackend.decode(token_str,verify=False)
            user = User.objects.get(id = valid_data['user_id'])
            user_object = self.serializer_class(user)
            json_data = JSONRenderer().render(user_object.data)
            return HttpResponse(json_data)
        except Exception:
            return HttpResponse(json.dumps({"error": "Invalid or expired token"}))