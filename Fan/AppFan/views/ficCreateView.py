import json
from rest_framework.renderers import JSONRenderer
from django.http import HttpResponse
from AppFan.serializers.ficSerializers import FicSerializer
from AppFan.serializers.usuarioSerializers import usuarioSerializers
from AppFan.models.usuario import User
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.response import Response
from rest_framework import generics, status
from django.conf import settings
from rest_framework import status, views


class ficCreateView(views.APIView):
    queryset = User.objects.all()
    serializer_class = usuarioSerializers
    # permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        try:
            token_str = kwargs["token"]
            tokenBackend = TokenBackend(
                algorithm=settings.SIMPLE_JWT['ALGORITHM'])
            valid_data = tokenBackend.decode(token_str, verify=False)
            user = User.objects.get(id=valid_data['user_id'])
            user_object = self.serializer_class(user)
            user_id = user_object.data["id"]
            request.data["fk_id_autor"] = user_id
            serializer = FicSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return HttpResponse(json.dumps({"status": "Fic added succesfully"}))

        except Exception as e:
            print(e)
            return HttpResponse(json.dumps({"error": "Invalid or expired token. Fic not added"}))
