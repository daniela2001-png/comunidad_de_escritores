"""Fan URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from rest_framework_simplejwt.views import (
    TokenObtainPairView, TokenRefreshView)
from AppFan import views
from AppFan.views import show_info_fic
from AppFan.views.ficCreateView import ficCreateView
from AppFan.views.Comentario import ComentarioVista
urlpatterns = [
    path('', views.FilterFics.filter_fics),
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/<str:token>/', views.UserDetailView.as_view()),
    path('ficinfo/<int:pk>/', show_info_fic),
    # path('ficinfo/all/', show_all_fics),
    path('addfic/<str:token>/', ficCreateView.as_view()),
    path('create_comment/<int:id_fic>/', ComentarioVista.as_view()),
    path('user/', views.UserCreateView.as_view())
]
